"""Reading a list of PDF from nested folders
"""
from os import walk
from glob import glob


def get_pdf_list(root_folder: str) -> list:
    """Return a flattened list of PDFs"""
    folder_list = [x[0] for x in walk(root_folder)]
    list_pdf = [glob(f"{x}/*.pdf") for x in folder_list if glob(f"{x}/*.pdf")]
    return [i for s in list_pdf for i in s]


if __name__ == "__main__":
    print(get_pdf_list("../input"))
