# extracting-data-PDF

Following [Manning course](https://liveproject.manning.com/module/134_2_1/delivery-notes-data-entry-automation-with-python/1--iterating-through-subfolders-and-identifying-pdfs/1-1--getting-a-list-of-pdfs-for-data-extraction?).

Reference for [the table parser](http://theautomatic.net/2019/05/24/3-ways-to-scrape-tables-from-pdfs-with-python/)
